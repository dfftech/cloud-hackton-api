#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["docker_dotnetcore.csproj", "docker_dotnetcore/"]
RUN dotnet restore "docker_dotnetcore/docker_dotnetcore.csproj"
COPY . ./docker_dotnetcore
WORKDIR "/src/docker_dotnetcore"
RUN dotnet build "docker_dotnetcore.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "docker_dotnetcore.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "docker_dotnetcore.dll"]